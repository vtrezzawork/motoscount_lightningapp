public with sharing class MotoController {

    @AuraEnabled
    public static List<Moto__c> findMoto(Decimal minPrice, Decimal maxPrice, String searchString, Boolean isPrivate, Boolean isCarDealership) {
    	ENEL_UTIL_LogGenerator logger = new ENEL_UTIL_LogGenerator();
        logger.start(MotoController.class.getName(), 'findMoto');
	    logger.log('@@NTT MotoController START findMoto');
	    logger.log('@@NTT MotoController - findMoto - UserInfo.getSessionId()='    +   UserInfo.getSessionId());
	    logger.log('@@NTT MotoController - findMoto - UserInfo.getUserId()='       +   UserInfo.getUserId());

	    logger.log('@@NTT MotoController - findMoto - minPrice'			+   minPrice);
	    logger.log('@@NTT MotoController - findMoto - maxPrice'			+   maxPrice);
	    logger.log('@@NTT MotoController - findMoto - searchString'		+   searchString);
	    logger.log('@@NTT MotoController - findMoto - isPrivate'		+   isPrivate);
	    logger.log('@@NTT MotoController - findMoto - isCarDealership'  +   isCarDealership);
        String key = '%' + searchString + '%';
	    logger.log('@@NTT MotoController - findMoto - key'   +   key);
		List<Moto__c> allMoto = null;
		if( (isPrivate && isCarDealership) || (!isPrivate && !isCarDealership) ) {
			logger.log('@@NTT MotoController - findMoto - if1');
			allMoto = [SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        							FROM Moto__c
        							WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key)];
		} else if(isPrivate && !isCarDealership) {
			logger.log('@@NTT MotoController - findMoto - if1elseif1');
			allMoto = [SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        							FROM Moto__c
        							WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key AND Privato__c = true)];
		} else if(!isPrivate && isCarDealership) {
			logger.log('@@NTT MotoController - findMoto - if1elseif2');
			allMoto = [SELECT Id, Marca__c, Name, Foto__c, URL_Foto__c, Prezzo__c, Luogo__c 
        							FROM Moto__c
        							WHERE (Prezzo__c >=: minPrice AND Prezzo__c <=: maxPrice AND Marca__c LIKE : key AND Privato__c = false)];
		}
		logger.printList(allMoto);
	    logger.end();	    
	    return allMoto;
    }
}