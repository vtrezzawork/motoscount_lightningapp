({
	handleSelectedSObject: function (component, event, helper) {
		console.log("MotoTileDetail - handleSelectedSObject");
		var recordId = event.getParam("recordId");
		console.log("MotoTileDetail - handleSelectedSObject - recordId = " + recordId);
		component.set("v.recordId", recordId);
		component.find("motoID").reloadRecord();
	},

	handleFilterEvent: function(component, event, helper) {
		console.log("MotoTileDetail - handleFilterEvent");
		component.set("v.moto", '');
		component.set("v.motoFields", '');
	}
})