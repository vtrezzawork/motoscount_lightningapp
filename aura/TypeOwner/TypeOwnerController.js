({
	typeOwnerChange: function (component, event, helper) {
	console.log("typeOwnerChange START");
	var isPrivate = component.get("v.isPrivate");
	var isCarDealership = component.get("v.isCarDealership");
	console.log("typeOwnerChange - isPrivate = " + isPrivate);
	console.log("typeOwnerChange - isCarDealership = " + isCarDealership);
    var myEvent = $A.get("e.c:TypeOwnerChange");
    myEvent.setParams({
        "isPrivate": isPrivate,
		"isCarDealership": isCarDealership
    });
	myEvent.fire();
	}
})