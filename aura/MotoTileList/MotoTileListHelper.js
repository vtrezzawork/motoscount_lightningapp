({
	getMotos : function(component, page) {
		console.log("herlper - getMotos - START");
		console.log("herlper - getMotos - minPrice" + component.get("v.minPrice"));
		console.log("herlper - getMotos - maxPrice" + component.get("v.maxPrice"));
		console.log("herlper - getMotos - searchString" + component.get("v.searchString"));
		console.log("herlper - getMotos - isPrivate" + component.get("v.isPrivate"));
		console.log("herlper - getMotos - isCarDealership" + component.get("v.isCarDealership"));
        var action = component.get("c.findMoto");
        action.setParams({
      		"minPrice": component.get("v.minPrice"),
      		"maxPrice": component.get("v.maxPrice"),
			"searchString": component.get("v.searchString"),
      		"isPrivate": component.get("v.isPrivate"),
      		"isCarDealership": component.get("v.isCarDealership")
        });
    	action.setCallback(this, function(response) {
    		console.log("response.getState() = " + response.getState());
    		if(response.getState() == 'SUCCESS') {
    			console.log("response.getReturnValue() = " +JSON.stringify(response.getReturnValue()));
            	component.set("v.motos", response.getReturnValue());
    		}
    	});
    	$A.enqueueAction(action);
	}
})