({
	navigateToDetailsView : function(component) {
		var moto 	= component.get("v.moto");
        var myEvent = $A.get("e.force:navigateToSObject");
        myEvent.setParams({
            "recordId": moto.Id
        });
        myEvent.fire();
	},

	propertySelected : function(component) {
		var moto	= component.get("v.moto");
		console.log("MotoTile - propertySelected - moto = " + moto);
        var myEvent = $A.get("e.ltng:selectSObject");
        myEvent.setParams({"recordId": moto.Id, channel: "Properties"});
        myEvent.fire();
    }

})