<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
    <classAccesses>
        <apexClass>MotoController</apexClass>
        <enabled>false</enabled>
    </classAccesses>
    <custom>true</custom>
    <fieldPermissions>
        <editable>false</editable>
        <field>Moto__c.Foto__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.Km__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.Luogo__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.Marca__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.Prezzo__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.Privato__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <fieldPermissions>
        <editable>true</editable>
        <field>Moto__c.URL_Foto__c</field>
        <readable>true</readable>
    </fieldPermissions>
    <layoutAssignments>
        <layout>Account-Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Asset-Asset Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Attendee__c-Attendee Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Campaign-Campaign Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CampaignMember-Campaign Member Page Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Camping_Item__c-Camping Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Case-Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CaseClose-Close Case Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Certification__c-Certification Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>CollaborationGroup-Group Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contact-Contact Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ContentVersion-Content Version Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Contract-Contract Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Course__c-Course Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DandBCompany-D%26B Company Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Delivery__c-Delivery Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>DuplicateRecordSet-Duplicate Record Set Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>EmailMessage-Email Message Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Event-Event Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>FeedItem-Feed Item Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Figlio__c-Figlio Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Global-Global Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Goal-Goal Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>GoalLink-Goal Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Idea-Idea Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Interaction__c-Interaction Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>JobTracker-Job Tracker Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Lead-Lead Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Macro-Macro Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Metric-Metric Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>MetricDataLink-Metric Data Link Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Moto__c-Moto Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Opportunity-Opportunity Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OpportunityLineItem-Opportunity Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Order-Order Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>OrderItem-Order Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Padre__c-Padre Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Pricebook2-Price Book Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>PricebookEntry-Price Book Entry Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Privato__c-Privato Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Prodotto_Federico__c-Prodott Federico Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product2-Product Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Product_Daily_Update__c-Product Daily Update Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvaMD__c-ProvaMD Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvaTrigger1__c-ProvaTrigger1 Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvaTrigger__c-ProvaTrigger Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>ProvaVincenzo__c-ProvaVincenzo Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Session_Speaker__c-Session Speaker Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Session__c-Session Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPersona-Social Persona Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>SocialPost-Social Post Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Solution-Solution Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Speaker__c-Speaker Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Store__c-Store Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Task-Task Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Uno__c-Uno Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>User-User Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserAlt-User Profile Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvAccount-User Provisioning Account Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningLog-User Provisioning Log Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>UserProvisioningRequest-User Provisioning Request Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>Vehicle__c-Vehicle Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkCoaching-Coaching Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedback-Feedback Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestion-Feedback Question Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackQuestionSet-Feedback Question Set Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackRequest-Feedback Request Layout - Winter %2716</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkFeedbackTemplate-Feedback Template Layout</layout>
    </layoutAssignments>
    <layoutAssignments>
        <layout>WorkPerformanceCycle-Performance Cycle Layout - Winter %2716</layout>
    </layoutAssignments>
    <objectPermissions>
        <allowCreate>true</allowCreate>
        <allowDelete>true</allowDelete>
        <allowEdit>true</allowEdit>
        <allowRead>true</allowRead>
        <modifyAllRecords>false</modifyAllRecords>
        <object>Moto__c</object>
        <viewAllRecords>false</viewAllRecords>
    </objectPermissions>
    <tabVisibilities>
        <tab>Moto_Explorer</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <tabVisibilities>
        <tab>Moto__c</tab>
        <visibility>DefaultOn</visibility>
    </tabVisibilities>
    <userLicense>Salesforce Platform</userLicense>
    <userPermissions>
        <enabled>true</enabled>
        <name>AllowUniversalSearch</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ApiEnabled</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>AssignTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterEditOwnPost</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterFileLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInternalUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterInviteExternalUsers</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ChatterOwnGroups</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ContentWorkspaces</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeFilters</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateCustomizeReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CreateTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>CustomMobileAppsAccess</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>DistributeFromPersWksp</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditEvent</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTask</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EditTopics</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailMass</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EmailSingle</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableCommunityAppLauncher</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>EnableNotifications</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ExportReport</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ImportPersonal</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>LightningExperienceUser</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>MassInlineEdit</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>RunReports</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>SelectFilesFromSalesforce</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ShowCompanyNameAsUserBadge</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewHelpLink</name>
    </userPermissions>
    <userPermissions>
        <enabled>true</enabled>
        <name>ViewSetup</name>
    </userPermissions>
</Profile>
